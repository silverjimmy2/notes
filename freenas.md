## freenas

#### Format an external disk as UFS
  `sudo newfs /dev/da0p1`

#### Update plex jail
`service plexmediaserver stop`
`pkg update && pkg upgrade multimedia/plexmediaserver`
`service plexmediaserver start`

#### Helpful links:
Setting up Plex jail:
https://forums.freenas.org/index.php?threads/tutorial-how-to-install-plex-in-a-freenas-11-0-jail.19412/


###### Remove a line from a file (when the line contains slashes)
  `sed '\|^/dev/xvdb|d' /etc/fstab`
  Note: When you use a non-standard delimeter, the delimeter needs to be escaped
